
/*
 * Project: RepeatAgain
 * Author: So Chigusa
 * Date: 2019/1/30
 * File: main.cpp
 */

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>

int main() {
  std::cout << "Welcome to RepeatAgain" << std::endl;

  // output file name
  std::string ofname;
  std::cout << "Output shell script name: ";
  getline(std::cin, ofname);
  std::ofstream ofs(ofname);

  std::string buf;
  while(true) {
  
    // target source file name
    std::string src;
    std::cout << "Source file name: ";
    getline(std::cin, src);

    // target executable file name
    std::string exe;
    std::cout << "Executable file name: ";
    getline(std::cin, exe);

    // commands to be changed
    int n = 1, m, ncom, nrun;
    std::vector< std::vector<std::string> > com;
    while(true) {
      std::vector<std::string> vec;
    
      std::cout << "Command #" << n;
      if(n > 1) std::cout << " (Type 'fin' to finish)";
      std::cout << ": ";
      getline(std::cin, buf);

      if(n > 1 && buf == "fin") break;
      vec.push_back(buf);
    
      // command changes
      m = 2;
      std::cout << "Run #1: " << buf << std::endl;
      while(n == 1 || m <= nrun) {
	std::cout << "Run #" << m;
	if(n == 1) std::cout << " (Type 'fin' to finish)";
	std::cout << ": ";
	getline(std::cin, buf);

	if(n == 1 && buf == "fin") break;
	vec.push_back(buf);
	++m;
      }

      // store commands
      com.push_back(vec);
      if(n == 1) nrun = m-1;
    
      ++n;
    }

    // generate scripts
    ncom = n-1;

    int next;
    for(m = 0; m < nrun; ++m) {
      ofs << "make" << std::endl;
      ofs << "./" << exe << std::endl;

      next = (m+1) % nrun;
      for(n = 0; n < ncom; ++n) {
	ofs << "sed -ie 's|" << com[n][m] << "|" << com[n][next] << "|g' " << src << std::endl;
      }
    }
    
    // if want, continue editing other sources
    while(true) {
      std::cout << "Continue other source files? [y/n]: ";
      getline(std::cin, buf);
      if(buf != "y" && buf != "n") std::cout << "Please answer y or n" << std::endl;
      else break;
    }
    if(buf == "n") break;
  }
  ofs.close();
  
  // executability
  std::string sys = "chmod 755 "+ofname;
  system(sys.c_str());
  
  return 0;
}
